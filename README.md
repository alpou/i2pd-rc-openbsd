# I2Pd RC script for OpenBSD

Instructions on how to compile I2Pd on OpenBSD, together with a makefile and an
RC script to make it a system service, allowing it to be started at boot and so
on.


## Description

[I2Pd](https://i2pd.website/) is packaged in
[OpenBSD](https://www.openbsd.org/)'s official repositories, but the provided
versions are usually old, and older versions don't always run reliably when
interacting with newer versions on the I2P network.

More recent versions can be installed using the ports tree, but doing so is
pretty unpleasant, from my experience: CVS has to be used, but it's slow and
unintuitive; also X11 has to be installed in order to be able to build ports,
which makes no sense on a headless server... \<\/rant\>

Fortunately, compiling the latest version from source on OpenBSD is painless.
LibreSSL is supported, the official instructions work out of the box, and only a
small manual intervention is required to make the daemon start at system boot.

This repository contains instructions on how to compile I2Pd on OpenBSD together
with a makefile and an RC script to make it a system service. This allows it
to be started at boot, to be managed by `rcctl`, to be restarted if it crashes,
etc.


## Compiling and installing

This part is taken from
[the official instructions](https://i2pd.readthedocs.io/en/latest/devs/building/unix/#building-on-unix-systems)
for compiling and installing I2Pd on a generic Unix system.
If the instructions listed here differ from the official instructions,
prioritize the official instructions, unless a detail specific to OpenBSD is
stated here.

First, install
[a few dependencies](https://i2pd.readthedocs.io/en/latest/devs/building/requirements/).
Git is not listed as a dependency in the official build documentation, but it is
needed as it does not come with OpenBSD's base system:

    $ doas pkg_add boost cmake git

Next, clone their git repository if it was not already cloned before:

    $ cd /some/directory
    $ git clone https://github.com/PurpleI2P/i2pd.git
    $ cd i2pd

Or if the repository was already cloned before and you want to update it:

    $ cd /some/directory/i2pd
    $ git pull

Then, generate the makefile and compile the source. You can also
[pass options to cmake](https://i2pd.readthedocs.io/en/latest/devs/building/unix/#cmake-options)
to customize your makefile:

    $ cd build
    $ cmake .
    $ make

Next, install the binaries you just compiled:

    $ doas make install

Finally, the base config files and certificates must be copied in the
appropriate folders. This step is not clearly listed in the official
installation instructions. It is only mentionned
[on this page](https://i2pd.readthedocs.io/en/latest/user-guide/run/#recommended-way-to-run-i2pd-built-from-source).
These base files are in the `contrib` folder at the root of the git repository.

    $ cd ../contrib
    $ doas mkdir -p /etc/i2pd /var/lib/i2pd
    $ doas cp -R i2pd.conf tunnels.* /etc/i2pd
    $ doas cp -R certificates /var/lib/i2pd


## Making I2Pd start at boot

At this point, you can start I2Pd manually, but it won't start automatically
when the system boots. This section describes how to achieve this.

The first thing to do is to create a new login class to increase the number of
files I2Pd will be able to open simultaneously. By default, OpenBSD restricts
this number to a value that is too low for I2Pd to work properly. Symptoms of a
number that is too small include difficulties connecting to eepsites and high
CPU usage.

If you are using OpenBSD 7.0 or older, simply add the following lines at the
end of `/etc/login.conf` to create the login class:

    i2pd:\
        :openfiles-cur=102400:\
        :openfiles-max=102400:\
        :tc=daemon:

If you are using OpenBSD 7.1 or newer, a login class can be created by adding a
file in `/etc/login.conf.d`. The makefile ran in the next steps detects the
version and creates the file automatically if the version is 7.1 or newer.

This new login class will be used by the user `_i2pd` created in the following
steps.

Next, clone this git repository if it wasn't cloned before:

    $ cd /any/directory
    $ git clone https://gitlab.com/alpou/i2pd-rc-openbsd.git
    $ cd i2pd-rc-openbsd

Or if the repository was already cloned before and you want to update it:

    $ cd /any/directory/i2pd-rc-openbsd
    $ git pull

Then, simply run the makefile. This will create a dedicated user and group to run
I2Pd, both called `_i2pd`, and give them ownership of the binaries compiled
previously. The login class described above will also be created if you are using
OpenBSD 7.1 or newer, and the init script will be copied in `/etc/rc.d`.

    $ doas make install

The final step is to enable the service to make it start at boot time and then to
manually start it:

    $ doas rcctl enable i2pd
    $ doas rcctl start i2pd

You're done. Enjoy!


## Licensing

The content of this repository is provided under a 0BSD license. See the LICENSE
file for more information.
