# Note: Comments are unindented to not be printed during the installation

.PHONY: install uninstall
CLASS_FILE = /etc/login.conf.d/i2pd

install:
# This command adds a new login class allowing _i2pd to open more files
# simultaneously if the version of OpenBSD is 7.1 or newer. For older
# versions, the modification needs to be done manually in /etc/login.conf.
	@if [ $$(echo "$$(uname -r) >= 7.1" | bc) = 1 ]; then \
		echo "Creating i2pd login class"; \
		echo "i2pd:\\"                    >  $(CLASS_FILE); \
		echo "\t:openfiles-cur=102400:\\" >> $(CLASS_FILE); \
		echo "\t:openfiles-max=102400:\\" >> $(CLASS_FILE); \
		echo "\t:tc=daemon:"              >> $(CLASS_FILE); \
	fi

# This command verifies if the group _i2pd exists. If it doesn't, it creates
# it with gid 838.
	@if ! groupinfo -e _i2pd; then \
		echo "Creating _i2pd group"; \
		groupadd -g 838 _i2pd; \
	fi

# These commands verify if the user _i2pd exists. If it doesn't, it creates
# it with uid 838, default group 838 (which is _i2pd), login class "i2pd"
# (created manually before running this script), comment "i2pd account",
# home directory /var/lib/i2pd, and login shell /sbin/nologin (which forbids
# it from loging in). The mkdir and chown are used instead of the -m option
# of useradd in case /var/lib/i2pd already existed before the execution of
# this make target.
	@if ! userinfo -e _i2pd; then \
		echo "Creating _i2pd user"; \
		mkdir -p /var/lib/i2pd; \
		useradd -u 838 -g 838 -L i2pd -c "i2pd account" \
		-d /var/lib/i2pd -s /sbin/nologin _i2pd; \
		chown -R _i2pd:_i2pd /var/lib/i2pd; \
	fi

# These commands make the i2pd executable belong to the _i2pd user and
# group, and make it non-writable for all and non-executable for others.
	@echo "Giving ownership of i2pd to _i2pd"
	@chown _i2pd:_i2pd $$(which i2pd)
	@chmod 550 $$(which i2pd)

# This command copies the RC script in the RC script folder to allow I2Pd to
# be managed by the service supervisor.
	@echo "Installing i2pd service"
	@install -o root -g bin -m 555 i2pd /etc/rc.d/i2pd



uninstall:
# This command removes the RC script.
	@echo "Removing i2pd service"
	@rcctl disable i2pd
	@rm -f /etc/rc.d/i2pd

# These commands restore the ownership and permissions of i2pd.
	@echo "Giving ownership of i2pd back to root"
	@chown root:wheel $$(which i2pd)
	@chmod 755 $$(which i2pd)

# This command removes the _i2pd user and its home if they exist.
	@if userinfo -e _i2pd; then \
		echo "Removing _i2pd user"; \
		userdel -r _i2pd; \
	fi

# This command removes the _i2pd group if it exists.
	@if groupinfo -e _i2pd; then \
		echo "Removing _i2pd group"; \
		groupdel _i2pd; \
	fi

# This command removes the i2pd login class.
	@echo "Removing i2pd login class"
	@rm -f $(CLASS_FILE)


